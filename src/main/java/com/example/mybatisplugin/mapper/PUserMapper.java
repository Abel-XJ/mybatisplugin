package com.example.mybatisplugin.mapper;

import com.example.mybatisplugin.SimpleMapper;
import com.example.mybatisplugin.model.PUser;

import java.util.List;

public interface PUserMapper extends SimpleMapper<PUser> {
}