package com.example.mybatisplugin.mapper;

import com.example.mybatisplugin.SimpleMapper;
import com.example.mybatisplugin.model.PDevice;

public interface PDeviceMapper extends SimpleMapper<PDevice> {
}