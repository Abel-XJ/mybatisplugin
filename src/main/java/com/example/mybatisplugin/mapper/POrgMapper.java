package com.example.mybatisplugin.mapper;

import com.example.mybatisplugin.SimpleMapper;
import com.example.mybatisplugin.model.POrg;

public interface POrgMapper extends SimpleMapper<POrg> {
}