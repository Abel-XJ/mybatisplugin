package com.example.mybatisplugin.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "P_DEVICE")
public class PDevice {
    @Id
    @Column(name = "device_id")
    private Long deviceId;

    @Column(name = "user_id")
    private Long userId;

    private String account;

    @Column(name = "device_no")
    private String deviceNo;

    @Column(name = "created_time")
    private Date createdTime;

    @Column(name = "lastupdate_time")
    private Date lastupdateTime;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "lastupdate_by")
    private String lastupdateBy;

    private String status;

    /**
     * @return device_id
     */
    public Long getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return device_no
     */
    public String getDeviceNo() {
        return deviceNo;
    }

    /**
     * @param deviceNo
     */
    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    /**
     * @return created_time
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * @return lastupdate_time
     */
    public Date getLastupdateTime() {
        return lastupdateTime;
    }

    /**
     * @param lastupdateTime
     */
    public void setLastupdateTime(Date lastupdateTime) {
        this.lastupdateTime = lastupdateTime;
    }

    /**
     * @return created_by
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return lastupdate_by
     */
    public String getLastupdateBy() {
        return lastupdateBy;
    }

    /**
     * @param lastupdateBy
     */
    public void setLastupdateBy(String lastupdateBy) {
        this.lastupdateBy = lastupdateBy;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}