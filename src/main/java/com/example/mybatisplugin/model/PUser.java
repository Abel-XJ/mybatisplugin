package com.example.mybatisplugin.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "P_USER")
public class PUser {
    @Id
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_uuid")
    private Long userUuid;

    private String account;

    private String name;

    private String password;

    @Column(name = "org_id")
    private Long orgId;

    @Column(name = "org_name")
    private String orgName;

    @Column(name = "idCard_no")
    private String idcardNo;

    @Column(name = "idCard_no_show")
    private String idcardNoShow;

    private String phone;

    private String telephone;

    private String email;

    private String address;

    private String comment;

    @Column(name = "created_time")
    private Date createdTime;

    @Column(name = "lastupdate_time")
    private Date lastupdateTime;

    @Column(name = "created_user")
    private String createdUser;

    @Column(name = "lastupdate_user")
    private String lastupdateUser;

    private String status;

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return user_uuid
     */
    public Long getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid
     */
    public void setUserUuid(Long userUuid) {
        this.userUuid = userUuid;
    }

    /**
     * @return account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return org_id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * @param orgId
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * @return org_name
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return idCard_no
     */
    public String getIdcardNo() {
        return idcardNo;
    }

    /**
     * @param idcardNo
     */
    public void setIdcardNo(String idcardNo) {
        this.idcardNo = idcardNo;
    }

    /**
     * @return idCard_no_show
     */
    public String getIdcardNoShow() {
        return idcardNoShow;
    }

    /**
     * @param idcardNoShow
     */
    public void setIdcardNoShow(String idcardNoShow) {
        this.idcardNoShow = idcardNoShow;
    }

    /**
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return created_time
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * @return lastupdate_time
     */
    public Date getLastupdateTime() {
        return lastupdateTime;
    }

    /**
     * @param lastupdateTime
     */
    public void setLastupdateTime(Date lastupdateTime) {
        this.lastupdateTime = lastupdateTime;
    }

    /**
     * @return created_user
     */
    public String getCreatedUser() {
        return createdUser;
    }

    /**
     * @param createdUser
     */
    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    /**
     * @return lastupdate_user
     */
    public String getLastupdateUser() {
        return lastupdateUser;
    }

    /**
     * @param lastupdateUser
     */
    public void setLastupdateUser(String lastupdateUser) {
        this.lastupdateUser = lastupdateUser;
    }

    /**
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}