package com.example.mybatisplugin.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "P_ORG")
public class POrg {
    @Id
    @Column(name = "org_id")
    private Long orgId;

    @Column(name = "org_uuid")
    private Long orgUuid;

    @Column(name = "org_name")
    private String orgName;

    @Column(name = "org_parents_id")
    private Long orgParentsId;

    @Column(name = "org_comment")
    private String orgComment;

    @Column(name = "created_time")
    private Date createdTime;

    @Column(name = "lastupdate_time")
    private Date lastupdateTime;

    @Column(name = "created_user")
    private String createdUser;

    @Column(name = "lastupdate_user")
    private String lastupdateUser;

    /**
     * @return org_id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * @param orgId
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * @return org_uuid
     */
    public Long getOrgUuid() {
        return orgUuid;
    }

    /**
     * @param orgUuid
     */
    public void setOrgUuid(Long orgUuid) {
        this.orgUuid = orgUuid;
    }

    /**
     * @return org_name
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return org_parents_id
     */
    public Long getOrgParentsId() {
        return orgParentsId;
    }

    /**
     * @param orgParentsId
     */
    public void setOrgParentsId(Long orgParentsId) {
        this.orgParentsId = orgParentsId;
    }

    /**
     * @return org_comment
     */
    public String getOrgComment() {
        return orgComment;
    }

    /**
     * @param orgComment
     */
    public void setOrgComment(String orgComment) {
        this.orgComment = orgComment;
    }

    /**
     * @return created_time
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTime
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * @return lastupdate_time
     */
    public Date getLastupdateTime() {
        return lastupdateTime;
    }

    /**
     * @param lastupdateTime
     */
    public void setLastupdateTime(Date lastupdateTime) {
        this.lastupdateTime = lastupdateTime;
    }

    /**
     * @return created_user
     */
    public String getCreatedUser() {
        return createdUser;
    }

    /**
     * @param createdUser
     */
    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    /**
     * @return lastupdate_user
     */
    public String getLastupdateUser() {
        return lastupdateUser;
    }

    /**
     * @param lastupdateUser
     */
    public void setLastupdateUser(String lastupdateUser) {
        this.lastupdateUser = lastupdateUser;
    }
}