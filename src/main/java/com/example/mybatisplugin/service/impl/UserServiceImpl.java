package com.example.mybatisplugin.service.impl;

import com.example.mybatisplugin.mapper.PUserMapper;
import com.example.mybatisplugin.model.PUser;
import com.example.mybatisplugin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author XiaJun
 * @create 2018-04-12 15:28
 * @sinc v1.0.0
 **/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    PUserMapper pUserMapper;

    @Override
    public List<PUser> selectUserList() {

        return pUserMapper.selectAll();
    }
}
