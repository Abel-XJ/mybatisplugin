package com.example.mybatisplugin.service;

import com.example.mybatisplugin.model.PUser;

import java.util.List;

/**
 * @author XiaJun
 * @create 2018-04-12 15:28
 * @sinc v1.0.0
 **/
public interface UserService {

    public List<PUser> selectUserList();
}
