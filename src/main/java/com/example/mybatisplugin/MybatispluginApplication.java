package com.example.mybatisplugin;

import com.example.mybatisplugin.handler.SimpleMybatisInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Properties;

@EnableWebMvc
@SpringBootApplication
@MapperScan(basePackages = "com.example.mybatisplugin.mapper")
public class MybatispluginApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MybatispluginApplication.class, args);
	}

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MybatispluginApplication.class);
    }

    @Bean
    public SimpleMybatisInterceptor sqlStatsInterceptor(){
        SimpleMybatisInterceptor sqlStatsInterceptor = new SimpleMybatisInterceptor();
        Properties properties = new Properties();
        properties.setProperty("dialect", "mysql");
        sqlStatsInterceptor.setProperties(properties);
        return sqlStatsInterceptor;
    }

}
