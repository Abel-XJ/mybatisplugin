package com.example.mybatisplugin.controller;

import com.example.mybatisplugin.model.PUser;
import com.example.mybatisplugin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author XiaJun
 * @create 2018-04-12 15:29
 * @sinc v1.0.0
 **/
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping("/userList")
    public List<PUser> selectUserList(){

        return userService.selectUserList();
    }
}
