package com.example.mybatisplugin;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author XiaJun
 * @create 2018-04-12 14:41
 * @sinc v1.0.0
 **/
public interface SimpleMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
